# acme-lambda

## Summary

Creates an AWS Serverless Lambda written in Python that uses the [ACME](https://github.com/certbot/certbot/tree/master/acme) client from Let's Encrypt to automatically provision free SSL/HTTPS certificates.

The Lambda is scheduled to run daily via a CloudWatch trigger.

Pseudo code for Lambda:
* Load all `*.yml` configurations from an S3 bucket
* Each YML configuration contains parameters such as contact info, domain names, etc that control how certificates are provisioned
* Load (if missing then create) the required ACME account information
* Load (if missing then create) the private key for each domain
* Load existing certificates
* Checks to see if existing certificates need to be renewed based on a threshold
* If certificate is missing or needs to be renewed, uses the ACME python client to provision a new certificate using DNS validation with Route53
* Saves the certificate in the S3 bucket

Each consumer needs to provide their YML configuration using the [acme-lambda-config](https://gitlab.com/bixbots/cloud/terraform-modules/acme-lambda-config) terraform module.

Then each EC2 instance can use the [cloud-init-acme-nginx](https://gitlab.com/bixbots/cloud/terraform-modules/cloud-init-acme-nginx) terraform module to periodically download and configure the certificates in nginx.

## YML Configuration

```yaml
directory: https://acme-staging-v02.api.letsencrypt.org/directory
email: someone@example.com
zone_name: example.cloud
reuse_key: true
renew_threshold_days: 30
domains:
  - subject: example.cloud
    alternate_names:
      - "*.example.cloud"
```

| Name                 | Description                                                                                                                                                                                                                                                                                                   | Type           | Default | Required |
|:---------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------------|:--------|:---------|
| directory            | Specifies the Let's Encrypt directory to use. During development/testing, the staging directory should be used. Once verified, the production directory can be used.<br> - Stage: `https://acme-staging-v02.api.letsencrypt.org/directory`<br> - Production: `https://acme-v02.api.letsencrypt.org/directory` | `string`       | -       | yes      |
| email                | Specifies the email address used to create the ACME account. Used for CSRs.                                                                                                                                                                                                                                   | `string`       | -       | yes      |
| zone_id              | Specifies the ID of the Route53 zone to use when creating DNS records for validation. If both `zone_id` and `zone_name` are specified, then `zone_id` takes precedance.                                                                                                                                       | `string`       | -       | yes      |
| zone_name            | Specifies the name of the Route53 zone to use when creating DNS records for validation. If both `zone_id` and `zone_name` are specified, then `zone_id` takes precedance.                                                                                                                                     | `string`       | -       | yes      |
| reuse_key            | Specifies if renewals should reuse the same private key or generate a new private key.                                                                                                                                                                                                                        | `bool`         | `true`  | no       |
| renew_threshold_days | Specifies the number of days before a certificate is about expire should it be renewed.                                                                                                                                                                                                                       | `number`       | 30      | no       |
| domains              | Contains a list of certificates to provision.                                                                                                                                                                                                                                                                 | `list(object)` | -       | yes      |
| subject              | Contains the primary domain for the certificate. Make sure this value contains valid characters for a file name (i.e. don't specify a wildcard here).                                                                                                                                                         | `string`       | -       | yes      |
| alternate_names      | Contains a list of subject alternative names (i.e. SAN) to include in the certificate. Can be used for certificates with multiple domains, subdomains, or wildcards.                                                                                                                                          | `list(string)` | -       | no       |

## Certificate Files

Once the Lamba executes, the same folder which contained the YML configuration will also contain the following generated files:

* `account.json`
    - Contains the ACME account information
    - Do not edit or delete
* `{subject}.cert.pem`
    - Contains the provisioned certificate
    - Should be used by your consuming application (i.e. nginx)
* `{subject}.chain.pem`
    - Contains the provisioned certificate appended with CA chains
    - Should be used by your consuming application (i.e. nginx)
* `{subject}.key.pem`
    - Contains the private key for the certificate
    - Should be used by your consuming application (i.e. nginx)

## References

* https://letsencrypt.org/docs/client-options/
