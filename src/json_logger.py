#!/usr/bin/env python3

import logging
import traceback
import json
import time
import sys
import os

# Taken from: https://zoolite.eu/posts/lambda-python-logging/

class JSONFormatter(logging.Formatter):
    def format(self, record):
        string_formatted_time = time.strftime('%Y-%m-%dT%H:%M:%S', time.gmtime(record.created))
        obj = {
        	'message': record.msg,
        	'location': record.funcName,
            'category': record.name,
        	'level': record.levelname,
        	'time': f'{string_formatted_time}.{record.msecs:3.0f}Z',
        	'epoch_time': record.created
        }
        if hasattr(record, 'additional_info'):
            for key, value in record.additional_info.items():
                obj[key] = value
        return json.dumps(obj)

def setup_logger(logger_name = __name__):
    logger = logging.getLogger(logger_name)
    logger.propagate = False

    handler = logging.StreamHandler()
    formatter = JSONFormatter()
    handler.setFormatter(formatter)

    logger.addHandler(handler)

    if 'DEBUG' in os.environ and os.environ['DEBUG'] == 'true':
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        logging.getLogger('boto3').setLevel(logging.WARNING)
        logging.getLogger('botocore').setLevel(logging.WARNING)

    return logger

def log_unhandled_exception(logger):
    ex_type, ex_value, ex_traceback = sys.exc_info()

    extra = {
        'additional_info': {
            'exception': {
                'type': str(ex_type.__name__),
                'trace': []
            }
        }
    }

    tb_list = traceback.extract_tb(ex_traceback)
    trace_list = extra['additional_info']['exception']['trace']
    for tb_info in tb_list:
        filename, linenum, funcname, source = tb_info
        trace_list.append({
            'file': str(filename),
            'line': str(linenum),
            'function': str(funcname),
            'source': str(source)
        })

    logger.error(str(ex_value), extra=extra)

class LambdaContextLoggerAdapter(logging.LoggerAdapter):
    def process(self, msg, kwargs):
        if 'context' in self.extra:
            context = self.extra['context']
            dict = kwargs.setdefault('extra',{}).setdefault('additional_info',{}).setdefault('lambda',{})
            dict['function_name'] = context.function_name
            dict['function_version'] = context.function_version
            dict['aws_request_id'] = context.aws_request_id
        return msg, kwargs
