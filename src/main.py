#!/usr/bin/env python3

import sys
import json
import base64
import boto3
import hashlib
import logging
import os
import datetime
import requests
import yaml
import posixpath
import josepy as jose
import OpenSSL
import json_logger
import traceback
import collections

import acme.client
import acme.crypto_util
import acme.errors
import acme.messages
import acme.challenges

from contextlib import contextmanager
from botocore.config import Config
from botocore.exceptions import ClientError
from time import sleep

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa

USER_AGENT = 'acme-lambda'
ACC_KEY_SIZE = 2048
CERT_PKEY_BITS = 2048

LOG = json_logger.setup_logger(USER_AGENT)

def read_file(path):
  with open(path, 'r') as file:
    contents = file.read()
  return contents

def save_to_s3(config, path, content, content_type, kms_key='AES256'):
    s3 = config['s3']
    bucket_name = config['bucket_name']
    base_key = config.get('base_key', '')
    path = posixpath.join(base_key, path)
    debug_path = 'S3://{0}/{1}'.format(bucket_name, path)

    content_bytes = content
    if isinstance(content, str):
        content_bytes = content.encode('utf-8')

    content_md5 = base64.b64encode(hashlib.md5(content_bytes).digest()).decode('utf-8')

    if content_type is None:
        content_type = 'binary/octet-stream'

    LOG.debug('[S3] Saving object to "{0}"'.format(debug_path))
    kwargs = {
        'Bucket': bucket_name,
        'Key': path,
        'Body': content,
        'ContentType': content_type,
        'ContentMD5': content_md5,
        'ACL': 'private'
    }

    if kms_key != 'AES256':
        kwargs['ServerSideEncryption'] = 'aws:kms'
        kwargs['SSEKMSKeyId'] = kms_key
    else:
        kwargs['ServerSideEncryption'] = 'AES256'

    try:
        s3.put_object(**kwargs);
    except ClientError as e:
        LOG.error('[S3] Failed to save object "{0}"'.format(debug_path), extra={'additional_info': {'details': str(e) }})
        raise

def load_from_s3(config, path, ignore_error=False):
    s3 = config['s3']
    bucket_name = config['bucket_name']
    base_key = config.get('base_key', '')
    path = posixpath.join(base_key, path)
    debug_path = 'S3://{0}/{1}'.format(bucket_name, path)

    LOG.debug('[S3] Getting object from "{0}"'.format(debug_path))
    try:
        content = s3.get_object(Bucket=bucket_name, Key=path)['Body'].read()
    except ClientError as e:
        if ignore_error:
            return None
        LOG.error('[S3] Failed to get object "{0}"'.format(debug_path), extra={'additional_info': {'details': str(e) }})
        raise

    return content

def load_config(event, config_key):
    region = event['region']
    bucket_name = event['bucket_name']

    debug_path = 'S3://{0}/{1}'.format(bucket_name, config_key)
    LOG.info('Retrieving configuration from "{0}"'.format(debug_path))

    config = load_from_s3(event, config_key)

    config = yaml.load(config)

    # merge the dictionaries
    config = {**event, **config}

    missing = []

    if 'directory' not in config:
        missing.append('directory')

    if 'email' not in config:
        missing.append('email')

    if 'domains' not in config:
        missing.append('domains')

    if missing:
        raise Exception('Missing configuration values for "{0}"'.format(','.join(missing)))

    route53 = config['route53']
    load_zone_id(route53, config)

    return config

def load_zone_id(route53, dict):
    if 'zone_id' in dict:
        return dict['zone_id']

    if 'zone_name' not in dict:
        return None

    zone_name = dict['zone_name']
    zone_id = route53_get_zone_id(route53, zone_name)

    if zone_id is None:
        return None

    dict['zone_id'] = zone_id
    return zone_id

def load_account(config):
    LOG.info('Attempting to load account from S3')

    account = load_from_s3(config, 'account.json', True)
    if account == None:
        LOG.debug('Creating new account')
        account_key = jose.JWKRSA(key = rsa.generate_private_key(public_exponent = 65537, key_size=ACC_KEY_SIZE, backend = default_backend()))
        net = acme.client.ClientNetwork(account_key, user_agent=USER_AGENT)
        directory = acme.messages.Directory.from_json(net.get(config['directory']).json())
        acme_client = acme.client.ClientV2(directory, net=net)
        registration = acme_client.new_account(acme.messages.NewRegistration.from_data(email=config['email'], terms_of_service_agreed=True))
        account = {
            'account_key': account_key.to_json(),
            'registration': registration.to_json()
        }
        save_to_s3(config, 'account.json', json.dumps(account), 'application/json', config['kms_key'])
    else:
        LOG.debug('Found existing account')
        account = json.loads(account)
        account_key = jose.JWK.from_json(account['account_key'])
        net = acme.client.ClientNetwork(account_key, user_agent=USER_AGENT)
        directory = acme.messages.Directory.from_json(net.get(config['directory']).json())
        acme_client = acme.client.ClientV2(directory, net=net)
        registration = acme.messages.RegistrationResource.from_json(account['registration'])
        registration = acme_client.query_registration(registration)

    config['acme'] = acme_client
    config['account_key'] = account_key
    config['registration'] = registration

def try_process_domain(config, domain):
    try:
        return process_domain(config, domain)
    except:
        json_logger.log_unhandled_exception(LOG)
        return False

def process_domain(config, domain):
    route53 = config['route53']
    acme_client = config['acme']
    account_key = config['account_key']

    if 'subject' not in domain:
        LOG.error('Missing parameter "subject" for domain. Skipping')
        return False

    subject = domain['subject']

    if 'alternate_names' not in domain:
        domain['alternate_names'] = []

    if 'zone_id' in config and 'zone_id' not in domain:
        domain['zone_id'] = config['zone_id']

    if 'zone_name' in config and 'zone_name' not in domain:
        domain['zone_name'] = config['zone_name']

    if 'kms_key' not in domain:
        domain['kms_key'] = config['kms_key']

    if 'renew_threshold_days' not in domain:
        domain['renew_threshold_days'] = config['renew_threshold_days']

    if 'reuse_key' not in domain:
        domain['reuse_key'] = config['reuse_key']

    zone_id = load_zone_id(route53, domain)
    if zone_id is None:
        LOG.error('[{0}] Unable to determine zone id for domain. Skipping'.format(subject))
        return False

    kms_key = domain['kms_key']
    reuse_key = domain['reuse_key']
    renew_threshold_days = domain['renew_threshold_days']
    alternate_names = domain['alternate_names']
    subject_path_key = subject + '.key.pem'
    subject_path_cert = subject + '.cert.pem'
    subject_path_chain = subject + '.chain.pem'

    LOG.info('[{0}] Checking for existing certificate'.format(subject))
    cert_pem = load_from_s3(config, subject_path_cert, True)
    if not cert_pem is None:
        cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, cert_pem)
        not_after_x509 = cert.get_notAfter().decode('ascii')
        not_after = datetime.datetime.strptime(not_after_x509, '%Y%m%d%H%M%SZ')
        not_after = not_after.replace(tzinfo=datetime.timezone.utc)
        now = datetime.datetime.now(datetime.timezone.utc)
        days_remaining = (not_after - now).days
        LOG.info('[{0}] Found existing certificate; NotAfter="{1}"'.format(subject, not_after))
        LOG.info('[{0}] renew_threshold_days={1}; days_remaining={2}'.format(subject, renew_threshold_days, days_remaining))

        if days_remaining > renew_threshold_days:
            LOG.info('[{0}] Skipping renewal'.format(subject))
            return True
        else:
            LOG.info('[{0}] Renewing certificate'.format(subject))

    domains = [subject]
    if alternate_names and len(alternate_names) > 0:
        domains = domains + alternate_names

    if reuse_key:
        LOG.info('[{0}] Attempting to load private key from S3'.format(subject))
        pkey_pem = load_from_s3(config, subject_path_key, True)
        pkey_pem_isnew = not bool(pkey_pem)
    else:
        pkey_pem = None
        pkey_pem_isnew = True

    pkey_pem, csr_pem = new_csr_comp(domains, pkey_pem)

    if pkey_pem_isnew:
        LOG.info('[{0}] Saving private key to S3'.format(subject))
        save_to_s3(config, subject_path_key, pkey_pem, 'application/x-pem-file', kms_key)

    LOG.info('[{0}] ACME request starting'.format(subject))
    order_request = acme_client.new_order(csr_pem)

    LOG.info('[{0}] DNS challenge prepare'.format(subject))
    challenges = dns_challenges_prepare(config, order_request)
    if not challenges:
        LOG.error('[{0}] DNS challenge prepare failed'.format(subject))
        return False

    LOG.info('[{0}] DNS challenge setup'.format(subject))        
    changes = dns_challenges_setup(config, domain, challenges)
    if not changes:
        LOG.error('[{0}] DNS challenge setup failed'.format(subject))
        return False

    try:
        LOG.info('[{0}] DNS challenge wait'.format(subject))
        dns_challenges_wait(config, domain, changes)

        LOG.info('[{0}] DNS challenge finalize'.format(subject))
        if not dns_challenges_finalize(config, domain, challenges):
            LOG.error('[{0}] DNS challenge finalize failed'.format(subject))
            return False

    finally:
        LOG.info('[{0}] DNS challenge teardown'.format(subject))
        dns_challenges_teardown(config, domain, changes)

    LOG.info('[{0}] DNS challenge complete'.format(subject))

    try:
        LOG.info('[{0}] ACME request finalizing'.format(subject))
        order = acme_client.poll_and_finalize(order_request)

    except acme.errors.ValidationError as ex:
        for authzr in ex.failed_authzrs:
            msg = 'Authorization failed for "{0}"'.format(authzr.body.identifier.value)
            extra = {
                'additional_info': {
                    'failed_challenges': []
                }
            }
            for challenge in authzr.body.challenges:
                extra['additional_info']['failed_challenges'].append({
                    'challenge_type': challenge.chall.typ,
                    'error_type': challenge.error.typ if challenge.error else '',
                    'error_detail': challenge.error.detail if challenge.error else ''
                })
            LOG.error(msg, extra=extra)
        return False

    LOG.info('[{0}] Saving certificate chain to S3'.format(subject))
    save_to_s3(config, subject_path_chain, order.fullchain_pem, 'application/x-pem-file', kms_key)

    LOG.info('[{0}] Extracting primary certificate from chain'.format(subject))
    cert = OpenSSL.crypto.load_certificate(OpenSSL.crypto.FILETYPE_PEM, order.fullchain_pem)
    cert_pem = OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM, cert)

    LOG.info('[{0}] Saving primary certificate to S3'.format(subject))
    save_to_s3(config, subject_path_cert, cert_pem, 'application/x-pem-file', kms_key)

    return True

def new_csr_comp(domains, pkey_pem=None):
    if pkey_pem is None:
        pkey = OpenSSL.crypto.PKey()
        pkey.generate_key(OpenSSL.crypto.TYPE_RSA, CERT_PKEY_BITS)
        pkey_pem = OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_PEM, pkey)
    csr_pem = acme.crypto_util.make_csr(pkey_pem, domains)
    return pkey_pem, csr_pem

def dns_challenges_prepare(config, order_request):
        account_key = config['account_key']

        challenges = []
        for authorization in order_request.authorizations:
            for challenge_body in authorization.body.challenges:
                if isinstance(challenge_body.chall, acme.challenges.DNS01):

                    identifier_value = authorization.body.identifier.value
                    validation_domain_name = challenge_body.chall.validation_domain_name(identifier_value)
                    validation = challenge_body.chall.validation(account_key)

                    challenges.append({
                        'authorization_body': authorization.body,
                        'challenge_body': challenge_body,
                        'identifier_value': identifier_value,
                        'validation_domain_name': validation_domain_name,
                        'validation': validation,
                    })

        return challenges

def dns_challenges_setup(config, domain, challenges):
    account_key = config['account_key']
    route53 = config['route53']
    zone_id = domain['zone_id']
    subject = domain['subject']

    validation_domains = {}
    for challenge in challenges:
        validation_domain_name = challenge['validation_domain_name']

        if validation_domain_name in validation_domains:
            validation_domains[validation_domain_name].append(challenge)
        else:
            validation_domains[validation_domain_name] = [challenge]

    changes = []
    for validation_domain_name, challenges in validation_domains.items():
        resource_records = []

        for challenge in challenges:
            authorization_body = challenge['authorization_body']
            challenge_body = challenge['challenge_body']
            identifier_value = challenge['identifier_value']
            validation = challenge['validation']

            resource_record = {
                'Value': '"{0}"'.format(validation)
            }
            resource_records.append(resource_record)

        resource_record_set = {
            'TTL': 60,
            'Type': 'TXT',
            'Name': validation_domain_name,
            'ResourceRecords': resource_records
        }

        LOG.info('[{0}] Creating DNS record with TXT challenge for "{1}"'.format(subject, validation_domain_name))
        change_result = route53_change_record(route53, zone_id, 'UPSERT', resource_record_set)

        change = {
            'validation_domain_name': validation_domain_name,
            'resource_record_set': resource_record_set,
            'change_result': change_result,
            'challenges': challenges
        }

        changes.append(change)

    return changes

def dns_challenges_wait(config, domain, changes):
    account_key = config['account_key']
    route53 = config['route53']
    zone_id = domain['zone_id']
    subject = domain['subject']

    for change in changes:
        validation_domain_name = change['validation_domain_name']
        change_result = change['change_result']

        LOG.info('[{0}] Waiting for DNS to synchronize with new TXT challenge for "{1}"'.format(subject, validation_domain_name))
        change_result = route53_wait_for_change(route53, change_result)
        change['change_result'] = change_result

def dns_challenges_finalize(config, domain, challenges):
    account_key = config['account_key']
    acme_client = config['acme']
    subject = domain['subject']

    for challenge in challenges:
        authorization_body = challenge['authorization_body']
        challenge_body = challenge['challenge_body']
        identifier_value = challenge['identifier_value']
        validation_domain_name = challenge['validation_domain_name']
        validation = challenge['validation']

        challenge_response = challenge_body.chall.response(account_key)

        verified = challenge_response.simple_verify(challenge_body.chall, identifier_value, account_key)
        if not verified:
            LOG.error('[{0}] An error occurred while verifying the challenge for "{1}"'.format(subject, validation_domain_name))
            return False

        challenge_resource = acme_client.answer_challenge(challenge_body, challenge_response)
        challenge_body = challenge_resource.body
        challenge['challenge_body'] = challenge_body

        if challenge_body.error != None:
            LOG.error('[{0}] An error occurred while answering the challenge for "{1}"'.format(subject, validation_domain_name), extra={'additional_info': {'details': str(challenge_resource.body.error) }})
            return False

    return True

def dns_challenges_teardown(config, domain, changes):
    account_key = config['account_key']
    route53 = config['route53']
    zone_id = domain['zone_id']
    subject = domain['subject']

    for change in changes:
        validation_domain_name = change['validation_domain_name']
        resource_record_set = change['resource_record_set']

        LOG.info('[{0}] Deleting DNS record with TXT challenge for "{1}"'.format(subject, validation_domain_name))
        route53_change_record(route53, zone_id, 'DELETE', resource_record_set)

def route53_get_zone_id(route53, zone_name):
    if not zone_name.endswith('.'):
        zone_name += '.'

    try:
        dn = ''
        zi = ''
        zone_list = route53.list_hosted_zones_by_name(DNSName=zone_name)
        while True:
            for zone in zone_list['HostedZones']:
                if zone['Name'] == zone_name:
                    return zone['Id']

            if zone_list['IsTruncated'] is not True:
                return None

            dn = zone_list['NextDNSName']
            zi = zone_list['NextHostedZoneId']

            LOG.debug('[Route53] Continuing to fetch mode hosted zones...')
            zone_list = route53.list_hosted_zones_by_name(DNSName=dn, HostedZoneId=zi)

    except ClientError as e:
        LOG.error('[Route53] Failed to retrieve zone id for "{0}"'.format(zone_name), extra={'additional_info': {'details': str(e) }})
        return None

    return None

def route53_change_record(route53, zone_id, action, resource_record_set):
    change_batch = {
        'Comment': 'LetsEncrypt Lambda',
        'Changes': [{
            'Action': action,
            'ResourceRecordSet': resource_record_set
        }]
    }

    try:
        change_result = route53.change_resource_record_sets(HostedZoneId=zone_id, ChangeBatch=change_batch)
    except ClientError as e:
        LOG.error('[Route53] Failed to "{0}" record'.format(action), extra={'additional_info': {'details': str(e) }})
        raise

    return change_result

def route53_wait_for_change(route53, change_result):
    change_id = change_result['ChangeInfo']['Id']
    change_status = change_result['ChangeInfo']['Status']

    while change_status != 'INSYNC':
        try:
            sleep(5)
            change_result = route53.get_change(Id=change_id)
            change_status = change_result['ChangeInfo']['Status']

        except ClientError as e:
            LOG.error('[Route53] Failed to get change status', extra={'additional_info': {'details': str(e) }})
            raise

    return change_result

def try_process_config(event, config_key):
    try:
        return process_config(event, config_key)
    except:
        json_logger.log_unhandled_exception(LOG)
        return False

def process_config(event, config_key):
    LOG.info('[{0}] Process starting'.format(config_key))

    config = load_config(event, config_key)

    if 'base_key' not in config:
        head, tail = posixpath.split(config_key)
        config['base_key'] = head

    load_account(config)

    success = True
    for domain in config['domains']:
        success = try_process_domain(config, domain) and success

    if success:
        LOG.info('[{0}] Process completed'.format(config_key))
    else:
        LOG.info('[{0}] Process failed'.format(config_key))

    return success

def process_main(event):
    s3 = event['s3']
    bucket_name = event['bucket_name']

    config_keys = []
    paginator = s3.get_paginator('list_objects_v2')
    for page in paginator.paginate(Bucket=bucket_name):
        if 'Contents' not in page:
            continue
        contents = page['Contents']
        for obj in contents:
            key = obj['Key']
            if key.endswith('.yml'):
                config_keys.append(key)

    if not config_keys:
        LOG.info('Unable to find any "*.yml" files')
        return False

    for config_key in config_keys:
        event = event.copy()
        try_process_config(event, config_key)

    return True

def lambda_handler(event, context):
    global LOG
    if context is not None:
        LOG = json_logger.LambdaContextLoggerAdapter(LOG, {'context': context})

    try:
        if 'bucket_name' not in event:
            raise Exception('No bucket name has been provided')

        if 'region' not in event:
            if 'AWS_DEFAULT_REGION' not in os.environ:
                raise Exception('Unable to determine AWS region code')
            event['region'] = os.environ['AWS_DEFAULT_REGION']

        if 'kms_key' not in event:
            event['kms_key'] = 'AES256'

        if 'reuse_key' not in event:
            event['reuse_key'] = True

        if 'renew_threshold_days' not in event:
            event['renew_threshold_days'] = 30

        region = event['region']
        LOG.info('Using region "{0}"'.format(region))

        event['s3'] = boto3.client('s3', config=Config(signature_version='s3v4', region_name=region))
        event['route53'] = boto3.client('route53', config=Config(signature_version='v4', region_name=region))

        process_main(event)

    except:
        json_logger.log_unhandled_exception(LOG)

def main():
  request = json.load(sys.stdin)
  lambda_handler(request, None)

if __name__ == '__main__':
  main()
