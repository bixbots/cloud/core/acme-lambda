image:
  name: python:3.8.2-buster
  entrypoint: [""]

stages:
  - build
  - plan
  - apply
  - destroy

variables:
  AWS_DEFAULT_REGION: "us-east-1"
  TERRAFORM_VERSION: "0.12.24"

cache:
  paths:
    - src/.cache/pip
    - src/venv/

.terraform_base:
  before_script:
    - apt-get update -y > /dev/null
    - apt-get install -y wget unzip git > /dev/null
    - wget --quiet https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip > /dev/null
    - unzip -o terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin > /dev/null

.management_environment:
  variables:
    environment: management
  environment:
    name: $environment
  tags:
    - aws
    - us-east-1
    - management
    - docker
  when: manual
  allow_failure: true
  only:
    - master@bixbots/cloud/core/acme-lambda

build:
  stage: build
  when: always
  allow_failure: false
  extends:
    - .management_environment
  script:
    - apt-get update -y > /dev/null
    - apt-get install -y zip libyaml-dev python3-yaml > /dev/null
    - cd src
    - pip install -r requirements.txt -t . > /dev/null
    - zip -r ../terraform/lambda.zip . -x "bin/*" requirements.txt setup.cfg
  artifacts:
    paths:
      - terraform/lambda.zip
    expire_in: 7 days

plan:
  stage: plan
  extends:
    - .terraform_base
    - .management_environment
  dependencies:
    - build
  script:
    - cd terraform
    - terraform init -get=true -input=false
    - terraform plan -out=tfplan
  artifacts:
    paths:
      - terraform/tfplan
      - terraform/.terraform
    expire_in: 7 days

apply:
  stage: apply
  extends:
    - .terraform_base
    - .management_environment
  dependencies:
    - build
    - plan
  script:
    - cd terraform
    - terraform apply -input=false tfplan

destroy:
  stage: destroy
  extends:
    - .terraform_base
    - .management_environment
  dependencies:
    - build
  script:
    - cd terraform
    - terraform init -get=true -input=false
    - terraform destroy -input=false -auto-approve
