# lambda

data "aws_iam_policy_document" "assume_role_lambda" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "primary" {
  name               = local.common_name
  assume_role_policy = data.aws_iam_policy_document.assume_role_lambda.json
  tags               = module.tags_base.tags
}

resource "aws_iam_role_policy_attachment" "cloudwatch" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  role       = aws_iam_role.primary.id
}

# route53

data "aws_iam_policy_document" "route53" {
  statement {
    actions = [
      "route53:ChangeResourceRecordSets",
      "route53:ListHostedZones",
      "route53:ListHostedZonesByName",
      "route53:GetChange",
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "route53" {
  name   = "${local.common_name}-route53"
  policy = data.aws_iam_policy_document.route53.json
}

resource "aws_iam_role_policy_attachment" "route53" {
  policy_arn = aws_iam_policy.route53.arn
  role       = aws_iam_role.primary.id
}

# read_only

locals {
  read_only_policy_bucket_permissions = [
    "s3:ListBucket",
    "s3:ListBucketVersions",
    "s3:ListBucketMultipartUploads",
  ]

  read_only_policy_object_permissions = [
    "s3:GetObject",
    "s3:GetObjectVersion",
    "s3:ListMultipartUploadParts",
  ]
}

data "aws_iam_policy_document" "read_only" {
  statement {
    effect    = "Allow"
    actions   = local.read_only_policy_bucket_permissions
    resources = [module.s3_bucket.bucket_arn]
  }

  statement {
    effect    = "Allow"
    actions   = local.read_only_policy_object_permissions
    resources = ["${module.s3_bucket.bucket_arn}/*"]
  }
}

resource "aws_iam_policy" "read_only" {
  name   = "${local.bucket_name}-read-only"
  policy = data.aws_iam_policy_document.read_only.json
}
