locals {
  region = "us-east-1"

  application = "acme"
  environment = "management"
  role        = "lambda"
  lifespan    = "permanent"

  bucket_name = "bixbots-certificates"

  common_name          = "${local.application}-${local.environment}-${local.role}"
  lambda_function_name = local.common_name

  # 03:00 AM every day
  schedule_expression = "cron(0 3 * * ? *)"

  log_retention_days = 3

  tags = {}
}

module "tags_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v1"

  application = local.application
  environment = local.environment
  lifespan    = local.lifespan
  tags        = local.tags
}

module "s3_bucket" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/s3-bucket.git?ref=v1"

  region      = local.region
  bucket_name = local.bucket_name
  tags        = module.tags_base.tags

  versioning = {
    enabled    = true
    mfa_delete = false
  }

  admin_policy = {
    enabled     = true
    iam_role_id = aws_iam_role.primary.id
  }
}

resource "aws_cloudwatch_log_group" "lambda" {
  name              = "/aws/lambda/${local.lambda_function_name}"
  retention_in_days = local.log_retention_days
  tags              = module.tags_base.tags
}

resource "aws_lambda_function" "primary" {
  filename         = "${path.module}/lambda.zip"
  source_code_hash = filebase64sha256("${path.module}/lambda.zip")

  function_name = local.lambda_function_name
  handler       = "main.lambda_handler"
  runtime       = "python3.8"

  timeout     = "900"
  memory_size = "1024"
  role        = aws_iam_role.primary.arn

  environment {
    variables = {
      BUCKET_ID            = module.s3_bucket.bucket_id
      READ_ONLY_POLICY_ARN = aws_iam_policy.read_only.arn
    }
  }

  tags = module.tags_base.tags
}

resource "aws_cloudwatch_event_rule" "lambda" {
  name                = local.common_name
  schedule_expression = local.schedule_expression
}

resource "aws_lambda_permission" "cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.lambda.arn
  function_name = aws_lambda_function.primary.arn
}

resource "aws_cloudwatch_event_target" "lambda" {
  target_id = local.common_name
  rule      = aws_cloudwatch_event_rule.lambda.name
  arn       = aws_lambda_function.primary.arn

  input = <<EOF
{
  "region": "${local.region}",
  "bucket_name": "${module.s3_bucket.bucket_id}",
  "kms_key": "AES256"
}
EOF
}
